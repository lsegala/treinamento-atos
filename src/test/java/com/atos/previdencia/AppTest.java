package com.atos.previdencia;

import org.junit.Test;

import java.util.concurrent.ExecutionException;

import static com.atos.previdencia.App.fibonacci;
import static org.junit.Assert.assertEquals;

public class AppTest
{
    @Test
    public void shouldAnswerWithTrue() throws ExecutionException, InterruptedException {
        assertEquals(1, (int) fibonacci(1));
        assertEquals(2, (int) fibonacci(2));
        assertEquals(6, (int) fibonacci(3));
    }
}
