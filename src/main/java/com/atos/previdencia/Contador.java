package com.atos.previdencia;

public class Contador {
    private Integer indice;

    public Integer getIndice() {
        return indice;
    }

    public void incrementar(){
        if(this.indice == null) this.indice = 0;
        this.indice++;
    }
}
