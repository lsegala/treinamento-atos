package com.atos.previdencia;

import java.util.stream.IntStream;

public class SomaNumeros {
    private Contador contador;

    /*
    public int somar(int limite){
        int resultado = 0;
        contador = new Contador();
        for(int i = 1; i <= limite; i++){
            contador.incrementar();
            resultado += contador.getIndice();
        }
        return resultado;
    }*/

    public int somar(int limite){
        Contador contador = new Contador();
        return IntStream.rangeClosed(1, limite)
                .map(i -> {
                    contador.incrementar();
                    return contador.getIndice();
                })
                .sum();
    }
}
