package com.atos.previdencia;

import java.util.Random;

/**
 * Hello world!
 *
 */
public class App 
{
    public static Integer fibonacci(Integer limite){
        if(limite <= 2){
            return limite;
        }else{
            return fibonacci(limite - 1) + fibonacci(limite - 2) + limite;
        }
    }
}
