#Java

## Pré-requisitos

* Java - (https://www.java.com/pt_BR/download/help/windows_manual_download.xml)
* Maven - (https://maven.apache.org/download.cgi)
* Git - (https://git-scm.com/downloads)
* IDE
    * Eclipse (https://www.eclipse.org/downloads/)
    * Intellij Idea (https://www.jetbrains.com/pt-br/idea/download/)
    * VS Code (https://code.visualstudio.com/docs/languages/java)

Obs.: Adicione a pasta bin do Git, do Maven e do Java ao seu PATH

## Iniciando o projeto

Dentro de uma pasta execute o comando abaixo:
``` 
mvn -Dfile.encoding=UTF-8 -DinteractiveMode=false -DgroupId=com.atos.previdencia -DartifactId=treinamento-atos -Dversion=1.0-SNAPSHOT -DarchetypeGroupId=org.apache.maven.archetypes -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=RELEASE org.apache.maven.plugins:maven-archetype-plugin:RELEASE:generate
```